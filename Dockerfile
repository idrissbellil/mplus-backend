FROM python:3.10
ENV PYTHONUNBUFFERED 1
RUN mkdir /mplus
WORKDIR /mplus
COPY requirements.txt /mplus/
RUN pip install -r requirements.txt
COPY . /mplus/
