from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase


class UsersTest(APITestCase):

    def test_when_the_user_is_created_an_authtoken_is_created_too(self):
        user = User.objects.create_user(username="user-a", password="user-a")
        self.assertTrue(Token.objects.get(user=user))
