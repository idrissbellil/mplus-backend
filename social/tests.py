import typing

from django.contrib.auth.models import Group, User
from django.urls import reverse
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase

from . import models


def _create_users(cls: typing.Type[APITestCase]) -> None:
    (group_user, _) = Group.objects.get_or_create(name="user")
    (group_admin, _) = Group.objects.get_or_create(name="admin")

    user_admin = User.objects.create_superuser(
        username="admin", email="", password="admin")
    user_admin.groups.add(group_admin)

    user_dummy = User.objects.create_user(username="user1", password="user1")
    user_dummy.groups.add(group_user)

    cls.admin = user_admin
    cls.user1 = user_dummy


def _init_db(cls):
    cls.profile = models.Profile.objects.create(
        name='user1', email='email@example.com', description='User 1 profile')
    cls.circle = models.Circle.objects.create(
        name='technology', description='Tech circle')
    cls.post = models.Post.objects.create(
        title='QUESTION: how get all TODOs?',
        text='how to get all TODOs in vim',
        profile=cls.profile,
        circle=cls.circle,
        likes=0,
    )
    cls.comment = models.Comment.objects.create(
        profile=cls.profile, post=cls.post, text='use regex with perl')


class ModelsLogicTest(APITestCase):

    @classmethod
    def setUpTestData(cls):
        _create_users(cls)
        _init_db(cls)

    def setUp(self):
        token = Token.objects.get(user=self.user1)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

    def test_when_a_circle_is_fetched_then_posts_are_fetched(self):
        response = self.client.get(reverse('circle-detail', args=(1,)))
        self.assertEqual(
            {
                'url': 'http://testserver/social/circle/1/',
                'name': 'technology',
                'description': 'Tech circle',
                'posts': ['http://testserver/social/post/1/']
            },
            response.json(),
        )

    def test_when_a_post_is_fetched_then_comments_are_fetched(self):
        response = self.client.get(reverse('post-detail', args=(1,)))
        self.assertEqual(
            {
                'title': 'QUESTION: how get all TODOs?',
                'text': 'how to get all TODOs in vim',
                'circle': 1,
                'created_date': self.post.created_date.strftime(
                    "%Y-%m-%dT%H:%M:%S.%fZ"),
                'likes': 0,
                'profile': 1,
                'url': 'http://testserver/social/post/1/',
                'comments': ['http://testserver/social/comment/1/'],
            },
            response.json(),
        )

    def test_when_profile_is_fetched_then_posts_are_fetched(self):
        response = self.client.get(reverse('profile-detail', args=(1,)))
        self.assertEqual(
            {
                'description': 'User 1 profile',
                'email': 'email@example.com',
                'name': 'user1',
                'posts': ['http://testserver/social/post/1/'],
                'url': 'http://testserver/social/profile/1/',
            },
            response.json(),
        )
