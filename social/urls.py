from django.urls import include, path
from rest_framework.routers import DefaultRouter
from rest_framework.schemas import get_schema_view

from . import views

schema_view = get_schema_view(title='M+ API')

router = DefaultRouter()
router.register('community', views.CommunityViewSet)
router.register('profile', views.ProfileViewSet)
router.register('circle', views.CircleViewSet)
router.register('post', views.PostViewSet)
router.register('comment', views.CommentViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('schema/', schema_view),
]
