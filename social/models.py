from django.db import models


class Profile(models.Model):
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    description = models.CharField(max_length=100)


class Circle(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1000)


class Community(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1000)
    profiles = models.ManyToManyField(Profile)


class Post(models.Model):
    title = models.CharField(max_length=100)
    text = models.CharField(max_length=1000)
    profile = models.ForeignKey(
        Profile,
        related_name='posts',
        on_delete=models.CASCADE,
    )
    circle = models.ForeignKey(
        Circle,
        related_name='posts',
        on_delete=models.CASCADE,
    )
    likes = models.IntegerField()
    created_date = models.DateTimeField(
        'date created',
        auto_now_add=True,
        blank=True,
    )


class Comment(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    post = models.ForeignKey(
        Post,
        related_name='comments',
        on_delete=models.CASCADE,
    )
    text = models.CharField(max_length=1000)
    created_date = models.DateTimeField(
        'date updated',
        auto_now_add=True,
        blank=True,
    )
