from rest_framework import serializers

from . import models


class ProfileSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='profile-detail')
    posts = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='post-detail'
    )

    class Meta:
        model = models.Profile
        fields = ('url', 'name', 'email', 'description', 'posts')


class CommentSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='comment-detail')

    class Meta:
        model = models.Comment
        fields = ('url', 'profile', 'post', 'text', 'created_date')


class PostSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='post-detail')
    profile = serializers.PrimaryKeyRelatedField(
        queryset=models.Profile.objects.all())
    circle = serializers.PrimaryKeyRelatedField(
        queryset=models.Circle.objects.all())
    comments = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='comment-detail'
    )

    class Meta:
        model = models.Post
        fields = (
            'circle',
            'comments',
            'created_date',
            'likes',
            'text',
            'title',
            'url',
            'profile',
        )


class CircleSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='circle-detail')
    posts = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='post-detail'
    )

    class Meta:
        model = models.Circle
        fields = ('url', 'name', 'description', 'posts')


class CommunitySerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='community-detail')
    profiles = ProfileSerializer(many=True, read_only=True)

    class Meta:
        model = models.Community
        fields = ('url', 'name', 'description', 'profiles')
