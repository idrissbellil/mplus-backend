# mplus backend

This the mplus social network backend.
Mplus project tries to get something as close as possible to G+.


# Run Locally Without Cloning the Project

```bash
cd /tmp/ && mkdir mplus && cd mplus
curl https://gitlab.com/idrissbellil/mplus-backend/-/raw/main/docker-compose.yml -o docker-compose.yml
docker-compose up
```

By default it will listen to `0.0.0.0:8000`.
